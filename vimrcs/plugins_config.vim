
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" OmniCppComplete
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Generate tags with Ctrl-F12
map <C-F12> :!ctags -R --sort=yes --c++-kinds=+p --fields=+iaS --extra=+q .<CR>
 "Recursively look for tags
set tags=./tags;

let OmniCpp_NamespaceSearch = 1
let OmniCpp_GlobalScopeSearch = 1
let OmniCpp_ShowAccess = 1
let OmniCpp_ShowPrototypeInAbbr = 1 " show function parameters
let OmniCpp_MayCompleteDot = 1 " autocomplete after .
let OmniCpp_MayCompleteArrow = 1 " autocomplete after ->
let OmniCpp_MayCompleteScope = 1 " autocomplete after ::
let OmniCpp_DefaultNamespaces = ["std", "_GLIBCXX_STD"]

set tags+=$HOME/.vim/cpp_tags
set tags+=$HOME/.vim/asio_tags
set path=.,,,** "Path is buffer path, current path (<**> means recursive)


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Tagbar
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <F4> :TagbarToggle<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Tagbar
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <Leader>u :UndotreeToggle<CR>
" Try persistent undo
try
if has("nvim")
    execute ':silent !mkdir -p ~/.vim/.undodir_nvim'
    set undodir=$HOME/.vim/.undodir_nvim
else
    execute ':silent !mkdir -p ~/.vim/.undodir'
    set undodir=$HOME/.vim/.undodir
endif
    set undofile
    set undolevels=500
catch
endtry

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" CtrlP / FZF
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" map <Leader>b :CtrlPBuffer<CR>
" map <Leader>w :CtrlPCurWD<CR>
map <Leader>b :Buffers<CR>
map <Leader>w :Files<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" NerdTree
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <F7> :NERDTreeFind<CR>

let g:NERDTreeDirArrowExpandable = '*'
let g:NERDTreeDirArrowCollapsible = '+'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Tagbar
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:tagbar_iconchars = ['*', '>']

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ConqueGdb
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" let g:ConqueGdb_Leader = '-'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Fugitive
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" This maps .. with edit %:h that will open the parent tree of the git
""" object being inspected
autocmd User fugitive
  \ if fugitive#buffer().type() =~# '^\%(tree\|blob\)$' |
  \   nnoremap <buffer> <Leader>.. :edit %:h<CR> |
  \ endif

""Grep -r in current directory
nnoremap <Leader>f :Ggrep <cword> <CR>
nnoremap <Leader>grb :Git rebase -i main <CR>
nnoremap <Leader>grbc :Git rebase --continue <CR>
nnoremap <Leader>ga :Git add % <CR>
nnoremap <Leader>gc :Git commit -s <CR>

""" Optional: delete buffers when closed to avoid bloating the buffer list
""" autocmd BufReadPost fugitive://* set bufhidden=delete

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Notes
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:notes_directories = ['/home/amorenoz/Groups Hive/Notes', '~/Documents/notes']
:let g:notes_smart_quotes = 0


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" YCM
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""nmap <Leader>cg :YcmCompleter GoTo <C-R>=expand("<cword>")<CR><CR>
"""nmap <Leader>ci :YcmCompleter GoToInclude <C-R>=expand("<cword>")<CR><CR>
"""nmap <Leader>cd :YcmCompleter GoToDeclalation <C-R>=expand("<cword>")<CR><CR>
"""nmap <Leader>cf :YcmCompleter GoToDefinition <C-R>=expand("<cword>")<CR><CR>
let g:ycm_global_ycm_extra_conf = '~/.vim/bundle/YouCompleteMe/third_party/ycm/.ycm_extra_conf.py'
"""let g:ycm_collect_identifiers_from_tags_files = 1 " Let YCM read tags from Ctags file
"""let g:ycm_use_ultisnips_completer = 1 " Default 1, just ensure
"""let g:ycm_seed_identifiers_with_syntax = 1 " Completion for programming language's keyword
"""let g:ycm_complete_in_comments = 1 " Completion in comments
"""let g:ycm_complete_in_strings = 1 " Completion in string
"""
"""let g:ycm_key_list_select_completion = ['<C-j>', '<Down>']
"""let g:ycm_key_list_previous_completion = ['<C-k>', '<Up>']

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Cscope
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" This tests to see if vim was configured with the '--enable-cscope' option
" when it was compiled.  If it wasn't, time to recompile vim... 
if has("cscope")

    """"""""""""" Standard cscope/vim boilerplate

    " use both cscope and ctag for 'ctrl-]', ':ta', and 'vim -t'

    if !has("nvim")
        set cscopetag
    endif

    " check cscope for definition of a symbol before checking ctags: set to 1
    " if you want the reverse search order.
    set csto=0

    " add any cscope database in current directory
    if filereadable("cscope.out")
        cs add cscope.out  
    " else add the database pointed to by environment variable 
    elseif $CSCOPE_DB != ""
        cs add $CSCOPE_DB
    endif

    " show msg when any other cscope db added
    set cscopeverbose  


    """"""""""""" My cscope/vim key mappings
    "
    " The following maps all invoke one of the following cscope search types:
    "
    "   's'   symbol: find all references to the token under cursor
    "   'g'   global: find global definition(s) of the token under cursor
    "   'c'   calls:  find all calls to the function name under cursor
    "   't'   text:   find all instances of the text under cursor
    "   'e'   egrep:  egrep search for the word under cursor
    "   'f'   file:   open the filename under cursor
    "   'i'   includes: find files that include the filename under cursor
    "   'd'   called: find functions that function under cursor calls
    "
    " Below are three sets of the maps: one set that just jumps to your
    " search result, one that splits the existing vim window horizontally and
    " diplays your search result in the new window, and one that does the same
    " thing, but does a vertical split instead (vim 6 only).
    "
    " I've used CTRL-\ and CTRL-@ as the starting keys for these maps, as it's
    " unlikely that you need their default mappings (CTRL-\'s default use is
    " as part of CTRL-\ CTRL-N typemap, which basically just does the same
    " thing as hitting 'escape': CTRL-@ doesn't seem to have any default use).
    " If you don't like using 'CTRL-@' or CTRL-\, , you can change some or all
    " of these maps to use other keys.  One likely candidate is 'CTRL-_'
    " (which also maps to CTRL-/, which is easier to type).  By default it is
    " used to switch between Hebrew and English keyboard mode.
    "
    " All of the maps involving the <cfile> macro use '^<cfile>$': this is so
    " that searches over '#include <time.h>" return only references to
    " 'time.h', and not 'sys/time.h', etc. (by default cscope will return all
    " files that contain 'time.h' as part of their name).


    " To do the first type of search, hit 'CTRL-\', followed by one of the
    " cscope search types above (s,g,c,t,e,f,i,d).  The result of your cscope
    " search will be displayed in the current window.  You can use CTRL-T to
    " go back to where you were before the search.  
    "

    nmap <C-c>s :cs find s <C-R>=expand("<cword>")<CR><CR>	
    nmap <C-c>g :cs find g <C-R>=expand("<cword>")<CR><CR>	
    nmap <C-c>c :cs find c <C-R>=expand("<cword>")<CR><CR>	
    nmap <C-c>t :cs find t <C-R>=expand("<cword>")<CR><CR>	
    nmap <C-c>e :cs find e <C-R>=expand("<cword>")<CR><CR>	
    nmap <C-c>f :cs find f <C-R>=expand("<cfile>")<CR><CR>	
    nmap <C-c>i :cs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
    nmap <C-c>d :cs find d <C-R>=expand("<cword>")<CR><CR>	


    " Using 'CTRL-spacebar' (intepreted as CTRL-@ by vim) then a search type
    " makes the vim window split horizontally, with search result displayed in
    " the new window.
    "
    " (Note: earlier versions of vim may not have the :scs command, but it
    " can be simulated roughly via:
    "    nmap <C-@>s <C-W><C-S> :cs find s <C-R>=expand("<cword>")<CR><CR>	

    nmap <C-@>s :scs find s <C-R>=expand("<cword>")<CR><CR>	
    nmap <C-@>g :scs find g <C-R>=expand("<cword>")<CR><CR>	
    nmap <C-@>c :scs find c <C-R>=expand("<cword>")<CR><CR>	
    nmap <C-@>t :scs find t <C-R>=expand("<cword>")<CR><CR>	
    nmap <C-@>e :scs find e <C-R>=expand("<cword>")<CR><CR>	
    nmap <C-@>f :scs find f <C-R>=expand("<cfile>")<CR><CR>	
    nmap <C-@>i :scs find i ^<C-R>=expand("<cfile>")<CR>$<CR>	
    nmap <C-@>d :scs find d <C-R>=expand("<cword>")<CR><CR>	


    " Hitting CTRL-space *twice* before the search type does a vertical 
    " split instead of a horizontal one (vim 6 and up only)
    "
    " (Note: you may wish to put a 'set splitright' in your .vimrc
    " if you prefer the new window on the right instead of the left

    nmap <C-@><C-@>s :vert scs find s <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@><C-@>g :vert scs find g <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@><C-@>c :vert scs find c <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@><C-@>t :vert scs find t <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@><C-@>e :vert scs find e <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@><C-@>f :vert scs find f <C-R>=expand("<cfile>")<CR><CR>	
    nmap <C-@><C-@>i :vert scs find i ^<C-R>=expand("<cfile>")<CR>$<CR>	
    nmap <C-@><C-@>d :vert scs find d <C-R>=expand("<cword>")<CR><CR>


    """"""""""""" key map timeouts
    "
    " By default Vim will only wait 1 second for each keystroke in a mapping.
    " You may find that too short with the above typemaps.  If so, you should
    " either turn off mapping timeouts via 'notimeout'.
    "
    "set notimeout 
    "
    " Or, you can keep timeouts, by uncommenting the timeoutlen line below,
    " with your own personal favorite value (in milliseconds):
    "
    "set timeoutlen=4000
    "
    " Either way, since mapping timeout settings by default also set the
    " timeouts for multicharacter 'keys codes' (like <F1>), you should also
    " set ttimeout and ttimeoutlen: otherwise, you will experience strange
    " delays as vim waits for a keystroke after you hit ESC (it will be
    " waiting to see if the ESC is actually part of a key code like <F1>).
    "
    "set ttimeout 
    "
    " personally, I find a tenth of a second to work well for key code
    " timeouts. If you experience problems and have a slow terminal or network
    " connection, set it higher.  If you don't set ttimeoutlen, the value for
    " timeoutlent (default: 1000 = 1 second, which is sluggish) is used.
    "
    "set ttimeoutlen=100

    ""nmap <C-y>s :cs find s <C-R>=expand("<cword>")<CR><CR>
    " nmap <Leader>cs :cs find s <C-R>=expand("<cword>")<CR><CR>
    " nmap <Leader>cg :cs find g <C-R>=expand("<cword>")<CR><CR>
    " nmap <Leader>cc :cs find c <C-R>=expand("<cword>")<CR><CR>
    " nmap <Leader>cd :cs find d <C-R>=expand("<cword>")<CR><CR>
endif



