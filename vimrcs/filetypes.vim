""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Python
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:pymode_breakpoint = 0
let g:no_flake8_maps = 1
autocmd FileType python map <buffer> <F9> :call flake8#Flake8()<CR>
let python_space_error_highlight = 1
autocmd FileType python setlocal expandtab
autocmd FileType python setlocal softtabstop=4
autocmd FileType python setlocal shiftwidth=4
autocmd FileType python setlocal textwidth=79
autocmd FileType python setlocal tabstop=4
autocmd FileType python setlocal fileformat=unix
if !has("nvim")
    autocmd FileType python setlocal omnifunc=ale ""completion#OmniFunc
endif

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Markdown
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:markdown_folding = 1


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" C/C++
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" Open cflow in a new split
autocmd FileType c nmap <buffer> <F9> :new <bar> set buftype=nofile <bar> set syntax=cflow <bar> r !cflow "#:p" <CR>

let c_space_errors = 1
let g:clang_format#style_options = {
            \ "BasedOnStyle": "LLVM",
            \ "BreakBeforeBraces": "BS_Linux",
            \ "ColumnLimit": 79,
            \ "UseTab": "UT_Never",
            \ "IndentCaseLabels": "true",
            \ "PointerAlignment": "Left"}
let g:clang_format#detect_style_file = 1
" map to <Leader>a in C++ code
autocmd FileType c,cpp,hpp,h nnoremap <buffer><Leader>a :<C-u>ClangFormat<CR>
autocmd FileType c,cpp,hpp,h vnoremap <buffer><Leader>a :ClangFormat<CR>
" Allow to auto format all code when save it. 0 false
autocmd FileType c,cpp,hpp,h let g:clang_format#auto_format = 0
autocmd FileType c,cpp,hpp,h set foldmethod=syntax
autocmd FileType c,cpp,hpp,h set cc=79
autocmd FileType c,cpp,hpp,h set ts=4 sts=4 sw=4 expandtab
autocmd FileType c,cpp,hpp,h let b:ale_linters = ['clangtidy']
autocmd FileType c,cpp,hpp,h let b:ale_c_clangtidy_options = '-I include -I ovs/include -I ovs/lib'
autocmd FileType c,cpp,hpp,h let b:ale_cpp_clangtidy_options = '-I include -I ovs/include -I ovs/lib'

autocmd FileType cpp,hpp setlocal expandtab
autocmd FileType cpp,hpp setlocal softtabstop=2
autocmd FileType cpp,hpp setlocal shiftwidth=2

autocmd FileType c,cpp,hpp,h let b:ale_fixers = ['prettier', 'eslint']
""" Apply linux coding style if we're on the linux tree
let g:linuxsty_patterns = [ "/usr/src/", "/linux", "/retis"]

" Toggle auto formatting:
nmap <Leader>C :ClangFormatAutoToggle<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Yaml
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Rust
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if !has("nvim")
    autocmd FileType rust let b:ale_linters = ['analyzer']
endif
autocmd FileType rust set makeprg=cargo

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Markdown
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
au BufNewFile,BufFilePre,BufRead *.md set filetype=markdown
let g:markdown_fenced_languages = ['python', 'rust', 'c', 'bash=sh']
let g:markdown_syntax_conceal = 1
