"" Adrian Moreno's vimscript. Ideally this file will be inside my git
"" repository and symlinked to ~/.vimrc
"" NOTE: This configuration depends on vundle. If it's a new system, run:
"" git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle

" Leader map. Has to be defined before plugins are loaded!
let mapleader = ","

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" System requirements
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" black
" fzf
" ctags
" cflow

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Vundle specifics
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
filetype off                  " required!
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()
Bundle 'gmarik/vundle'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugins
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Bundle 'DirDiff.vim'
Bundle 'ludovicchabant/vim-gutentags'
Bundle 'majutsushi/tagbar'
Bundle 'mbbill/undotree'
" Deprecating in favor of fzf
" Bundle 'kien/ctrlp.vim'
Bundle 'junegunn/fzf'
Bundle 'junegunn/fzf.vim'
Bundle 'scrooloose/nerdtree'
Bundle 'AnsiEsc.vim'
Bundle 'tpope/vim-fugitive'
" Plugins for github
Bundle 'tpope/vim-rhubarb'

Bundle 'vim-airline/vim-airline'
Bundle 'vim-airline/vim-airline-themes'
Bundle 'flazz/vim-colorschemes'
Bundle 'w0ng/vim-hybrid'
Bundle 'vim-misc'
Bundle 'notes.vim'
Bundle 'christoomey/vim-tmux-navigator'
" ALE
if !has("nvim")
    let g:ale_completion_enabled = 1
    Bundle 'dense-analysis/ale'
endif
""" It's constantly causing warnings. Commenting out
""Bundle 'Conque-GDB'
""" let g:ConqueGdb_Leader = '-'
"" }} Vundle bundles


" Languages
"Bundle 'Valloric/YouCompleteMe'
"" Python
if !has("nvim")
    Bundle 'python-mode/python-mode'
    Bundle 'nvie/vim-flake8'
endif
Bundle 'vim-scripts/indentpython.vim'

"" C/CPP
""Bundle 'OmniCppComplete'
Bundle 'rhysd/vim-clang-format'
Bundle 'vivien/vim-linux-coding-style'

"" Typescript
Bundle 'leafgarland/typescript-vim'

""" Mark-down
Bundle 'tpope/vim-markdown' 

""" Go
Bundle 'fatih/vim-go'

""" Yaml
Bundle 'pedrohdz/vim-yaml-folds'


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" General
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
filetype plugin indent on
filetype plugin on

set encoding=utf-8

" Read files when modified externally
set autoread

" Man pages in Vim!
runtime! ftplugin/man.vim

" Fast saving
nmap <leader>w :w!<cr>
" :W sudo saves the file
" (useful for handling the permission-denied error)
command! W execute 'w !sudo tee % > /dev/null' <bar> edit! 

" No beeping please
set belloff=all

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" User interface
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" To display the status line always
set laststatus=2
"" Set 7 lines to the cursor - when moving vertically using j/k
set so=7

" Highlight search results
set hlsearch
map <F3> :set invhlsearch<CR>

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases
set smartcase

" Show lines
set number
set ruler
set background=dark

" Show matching brackets when text indicator is over them
set showmatch

set diffopt+=vertical

" Default Tabulation and indentation
set softtabstop=4
set tabstop=8
set autoindent
set shiftwidth=4
set smarttab
set smartindent
set expandtab

set number
"set notextmode
set hlsearch
set cino=(0
:syn on

"Scroll the tabs
map <F5> :tabp<CR>
map <F6> :tabn<CR>

" Smart way to move between windows
""" These maps are deprecated by vim-tmux-navigator.
""" Uncomment if not available for whatever reason.
""" map <C-j> <C-W>j
""" map <C-k> <C-W>k
""" map <C-h> <C-W>h
""" map <C-l> <C-W>l

"With spanish keyboard ^] for going to the tab is horribly painful
map <Leader>g <C-]>

" Enable wildmenu for command completion
set wildmenu

"Mouse support
set mouse=a
map <ScrollWheelUp> <C-Y>
map <ScrollWheelDown> <C-E>

"Show all hidden characters
function! ToggleShowHiddenChars()
        :set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
        :set list!
endfunction

" Show all trailing spaces
map <F8> /\s\+$

""nmap <C-y>s :cs find s <C-R>=expand("<cword>")<CR><CR>
nmap <Leader>cs :cs find s <C-R>=expand("<cword>")<CR><CR>
nmap <Leader>cg :cs find g <C-R>=expand("<cword>")<CR><CR>
nmap <Leader>cc :cs find c <C-R>=expand("<cword>")<CR><CR>
nmap <Leader>cd :cs find d <C-R>=expand("<cword>")<CR><CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Coding
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Remove the i in complete to disable recursive search with Ctrl-P
setglobal complete-=i

" Make Vim completion popup menu work
" https://vim.fandom.com/wiki/Make_Vim_completion_popup_menu_work_just_like_in_an_IDE
set completeopt=menuone,menu,longest,preview

" automatically open and close the popup menu / preview window with cursor
" hover
au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Colors
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Colors {
" vim hardcodes background color erase even if the terminfo file does
" not contain bce (not to mention that libvte based terminals
" incorrectly contain bce in their terminfo files). This causes
" incorrect background rendering when using a color theme with a
" background color.
let &t_ut=''

" Work-around incomplete terminfo databases
" Particulalry useful when under `screen`, which may or may not be attached to
" a physical terminal capable of 256color mode.
if match($TERMCAP, 'Co#256:') == 0 || match($TERMCAP, ':Co#256:') > 0
    set t_Co=256
endif

let simplecolor=$AM_VIM_SIMPLECOLOR
if simplecolor == '1'
    colorscheme desert
else
    colorscheme hybrid
endif

""" Remove vmlinux.h from greps
set wildignore+=vmlinux.h
