-- [[ init.lua ]]
current_path = debug.getinfo(1).source:match("@?(.*/)")
package.path =  package.path .. ";" .. current_path .. "/?.lua"
require('config')
