-- [[ plug.lua ]]
-- Plugin installation

return require('packer').startup(function()
    -- Packer can manage itself
    use 'wbthomason/packer.nvim'

    -- Nvim lspconfig
    use 'neovim/nvim-lspconfig' 

    -- RUST
    -- Rust-tools
    use 'mrcjkb/rustaceanvim'

    -- Python

    -- Cscope
    use 'dhananjaylatkar/cscope_maps.nvim'

    -- Treesitter
    use 'nvim-treesitter/nvim-treesitter'

    -- Indent-blakline
    use "lukas-reineke/indent-blankline.nvim"

end)
