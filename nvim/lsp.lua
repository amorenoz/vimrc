local nvim_lsp = require("lspconfig")

-- Mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
local lsp_opts = { noremap=true, silent=true }
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, lsp_opts)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, lsp_opts)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, lsp_opts)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, lsp_opts)

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local generic_on_attach = function(client, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local bufopts = { noremap=true, silent=true, buffer=bufnr }
  vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
  vim.keymap.set('n', '<leader>g', vim.lsp.buf.definition, bufopts)
  vim.keymap.set("n", "K", vim.lsp.buf.hover, bufopts)
  --vim.keymap.set('n', '<leader>s', vim.lsp.buf.hover, bufopts)
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
  --vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
  vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wl', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, bufopts)
  vim.keymap.set('n', '<space>fm', function()
      vim.lsp.buf.format { async = true }
      end, bufopts)
  vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, bufopts)
  vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, bufopts)
  vim.keymap.set('n', '<space>ca', vim.lsp.buf.code_action, bufopts)
end

local lsp_flags = {
  -- This is the default in Nvim 0.7+
  debounce_text_changes = 150,
}


-- PYTHON
-- requires: pip install python-lsp-server[all] TODO: automatic manage dependency
nvim_lsp.pylsp.setup {
    -- on_attach is a callback called when the language server attachs to the buffer
    on_attach = function (client, bufnr)
        generic_on_attach(client, bufnr)
    end,
    flags = lsp_flags,
    capabilities = caps,
    settings = {
        pylsp = {
            plugins = {
                -- Requires python-lsp-black
                black = { enabled = true },
                flake8 = { enabled = true },
                -- Requires python-lsp-ruff
                ruff = { enabled = true },
                pylint = { enabled = false },
                pyflakes = { enabled = false },
                -- Requires pylsp-mypy
                pylsp_mypy = {
                    enabled = true,
                }
            }
        }
    }
}

-- Rustaceanvim
vim.g.rustaceanvim = {
  -- Plugin configuration
  tools = {
  },
  -- LSP configuration
  server = {
    on_attach = function(client, bufnr)
      -- you can also put keymaps in here
      generic_on_attach(client, bufnr)
      vim.keymap.set('n', '<space>d', function()
          vim.cmd.RustLsp('openDocs')
      end,
      { silent = true, buffer=bufnr })
    end,
    default_settings = {
      -- rust-analyzer language server configuration
      ['rust-analyzer'] = {
        files = {
          excludeDirs = {
            -- FIXME: Inject this dynamically in a project-specific config file.
            'retis/src/core/probe/kernel/bpf/.out',
            'retis/src/core/probe/kernel/bpf/.out',
            'retis/src/core/probe/kernel/bpf/.out',
            'retis/src/core/probe/user/bpf/.out',
            'retis/src/collect/collector/skb/bpf/.out',
            'retis/src/collect/collector/ovs/bpf/.out',
            'retis/src/collect/collector/nft/bpf/.out',
            'retis/src/collect/collector/ovs/bpf/.out',
            'retis/src/collect/collector/ovs/bpf/.out',
            'retis/src/collect/collector/skb_drop/bpf/.out',
            'retis/src/collect/collector/ct/bpf/.out',
            'retis/src/collect/collector/skb_tracking/bpf/.out',
          },
        },
        check = {
            -- FIXME: Inject this dynamically in a project-specific config file.
            overrideCommand = { "make", "clippy", "RA=1" },
        },
      },
    },
  },
  -- DAP configuration
  dap = {
  },
}

nvim_lsp.clangd.setup {
    on_attach = function (client, bufnr)
        generic_on_attach(client, bufnr)
    end,
    flags = lsp_flags,
}

--" Setup Completion
--" See https://github.com/hrsh7th/nvim-cmp#basic-configuration
--lua <<EOF
--local cmp = require'cmp'
--cmp.setup({
--  -- Enable LSP snippets
--  snippet = {
--    expand = function(args)
--        vim.fn["vsnip#anonymous"](args.body)
--    end,
--  },
--  mapping = {
--    ['<C-p>'] = cmp.mapping.select_prev_item(),
--    ['<C-n>'] = cmp.mapping.select_next_item(),
--    -- Add tab support
--    ['<S-Tab>'] = cmp.mapping.select_prev_item(),
--    ['<Tab>'] = cmp.mapping.select_next_item(),
--    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
--    ['<C-f>'] = cmp.mapping.scroll_docs(4),
--    ['<C-Space>'] = cmp.mapping.complete(),
--    ['<C-e>'] = cmp.mapping.close(),
--    ['<CR>'] = cmp.mapping.confirm({
--      behavior = cmp.ConfirmBehavior.Insert,
--      select = true,
--    })
--  },
--
--  -- Installed sources
--  sources = {
--    { name = 'nvim_lsp' },
--    { name = 'vsnip' },
--    { name = 'path' },
--    { name = 'buffer' },
--  },
--})
--EOF
