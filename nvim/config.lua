-- [[ config.lua ]]
-- Main nvim-specific configuration entrypoint
vim.cmd('source ' .. os.getenv("HOME") .. '/.vimrc')

-- IMPORTS
require('plug') -- Plugins
require('opts')
require('lsp')
